(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
$(function() {
  var RangeScale, Task, Viewport, calculateAmount, canvas, clamp, clearCache, config, context, contextCache, count, day, draw, drawLabels, event, events, eventsCount, force, getScale, getZoomLevel, getZoomRange, hour, label, labelsBuffer, log10, minute, rand, ref, ruler, scaleText, second, tonow, triangle, updateViewport, viewport;
  config = {
    width: 320,
    height: 480
  };
  canvas = $('#canvas');
  scaleText = $('#scale');
  canvas.attr({
    width: config.width,
    height: config.height
  });
  context = canvas.get(0).getContext('2d');
  context.fillStyle = 'rgba(200,0,0,0.1)';
  contextCache = null;
  labelsBuffer = null;
  (clearCache = function() {
    labelsBuffer = {};
    return contextCache = {};
  })();
  triangle = function(s, e, l) {
    context.fillStyle = 'rgba(200,0,0,0.1)';
    context.beginPath();
    context.moveTo(config.width, s);
    context.lineTo(config.width - l, e);
    context.lineTo(config.width, e);
    context.fill();
    return context.closePath();
  };
  ruler = function(y, size, color) {
    var width;
    if (color == null) {
      color = '#111';
    }
    y = Math.floor(y);
    width = size * config.width;
    context.fillStyle = color;
    return context.fillRect(0, y, width * 0.4, 1);

    /*
    		do context.beginPath
    		context.moveTo 0, y
    		context.lineTo config.width * 0.1, y
    		config.
     */
  };
  label = function(y, size, text, color) {
    if (color == null) {
      color = '#333';
    }
    y = Math.floor(y);
    size *= 40;
    if (!(contextCache[y] >= size)) {
      contextCache[y] = size;
      return labelsBuffer[y] = {
        font: size + 'px sans-serif',
        fillStyle: color,
        fillText: [text, 1.5 * size, y + size]
      };
    }
  };
  drawLabels = function() {
    var cur, results, y;
    results = [];
    for (y in labelsBuffer) {
      cur = labelsBuffer[y];
      context.font = cur.font;
      context.fillStyle = cur.fillStyle;
      results.push(context.fillText.apply(context, cur.fillText));
    }
    return results;
  };
  getScale = require('./utils.coffee').getScale;
  ref = require('./dates.coffee'), RangeScale = ref.RangeScale, Viewport = ref.Viewport, Task = ref.Task;
  log10 = function(x) {
    return Math.log(x) / Math.log(10);
  };
  tonow = function(x) {
    return Date.now() + x;
  };
  second = 1e3;
  minute = 60 * second;
  hour = 60 * minute;
  day = 24 * hour;
  getZoomLevel = function(r) {
    var elta;
    elta = r.end - r.start;
    delta /= 10 * day;
    return 6 + log10(delta);
  };
  getZoomRange = function(v) {
    var zooml;
    zooml = getZoomLevel(v);
    return {
      start: zooml - 1,
      end: zooml
    };
  };

  /*
  	viewport =
  		start: tonow -10 * day
  		end: tonow 10 * day
  
  	viewport.range = getZoomRange viewport
   */
  viewport = new Viewport(tonow(-10 * days), tonow(10 * day));
  clamp = function(mi, ma, x) {
    return Math.max(mi, Math.min(ma, x));
  };
  calculateAmount = function(v, ev, margin) {
    var delta, max, min;
    if (margin == null) {
      margin = 1;
    }

    /*
    		s = v.range.start
    		e = v.range.end
    		delta_s = s - ev.level
    		delta_e = ev.level - e
    		if s <= ev.level <= e
    			1
    		else if s > ev.level
    			clamp 0, 1, 1 - (delta_s / margin)
    		else if e < ev.level
    			clamp 0, 1, 1 - (delta_e / margin)
    		else
    			0
     */
    delta = ev.level.scale - v.level.scale;
    min = -1 - margin;
    max = 0 + margin;
    if (delta < min || delta > max) {
      return 0;
    } else if ((-1 <= delta && delta <= 0)) {
      return 1;
    } else if (delta > 0) {
      return 1 - delta;
    } else {
      return Math.scale(min, -1, delta);
    }
  };
  event = function(s, e) {
    var tmp;
    s = tonow(s);
    e = tonow(e);
    if (e < s) {
      tmp = e;
      e = s;
      s = tmp;
    }
    return new Task(s, e);
  };

  /*
  		obj =
  			start: tonow s
  			end: tonow e
  		obj.level = getZoomLevel obj
  		obj
   */

  /*
  	events = [
  		(event 1*day, 3*day)
  		(event 2*day, 4*day)
  			(event -2*day, 15*day)
  	]
   */
  rand = function(x) {
    return (x / -2) + Math.random() * Math.abs(x);
  };
  eventsCount = 30;
  events = (function() {
    var results;
    results = [];
    while (eventsCount--) {
      results.push(event(day * rand(600), day * rand(600)));
    }
    return results;
  })();
  force = 1;
  updateViewport = function() {
    viewport.start -= force * hour * 2;
    viewport.end += force * hour * 10;
    return viewport.range = getZoomRange(viewport);
  };
  ($(document.body)).on('click', function() {
    return force = -force;
  });
  count = 0;
  draw = function() {
    var amount, amount1, amount2, amount3, delta, e, e_e, e_l, e_s, ev, i, j, k, len, level, next, prev, r, ref1, s, time2px, unitPX;
    clearCache();
    context.clearRect(0, 0, config.width, config.height);
    level = viewport.level, delta = viewport.delta;
    amount = level.scale % 1;
    unitPX = config.height / delta;
    scaleText.text(level.name);
    amount1 = 0;
    amount2 = 0;
    amount3 = 0;
    (function() {
      var max, min;
      min = Math.min;
      max = Math.max;
      amount1 = 0.5 - min(0.5, amount);
      amount2 = 0.5 + (1 - amount) * 0.5;
      return amount3 = amount;
    })();
    if (typeof debug !== "undefined" && debug !== null) {
      for (i = j = 0; j <= 3; i = ++j) {
        ruler(100 + i, amount, 'rgba(0,0,255,0.3)');
      }
    }
    ref1 = RangeScale.getPrevNext(level), prev = ref1[0], next = ref1[1];
    time2px = function(t) {
      return config.height * viewport.parse(t);
    };
    if (prev) {
      prev.doEachRuler(viewport, function(t) {
        var positionY;
        positionY = time2px(t);
        label(positionY, amount1, prev.format(viewport, t));
        return ruler(positionY, amount1);
      });
    }
    level.doEachRuler(viewport, function(t) {
      var positionY;
      positionY = time2px(t);
      label(positionY, amount2, level.format(viewport, t));
      return ruler(positionY, amount2);
    });
    if (next) {
      next.doEachRuler(viewport, function(t) {
        var positionY;
        positionY = time2px(t);
        label(positionY, amount3, next.format(viewport, t));
        return ruler(positionY, amount3);
      });
    }
    for (k = 0, len = events.length; k < len; k++) {
      ev = events[k];
      s = viewport.start;
      e = viewport.end;
      r = e - s;
      e_s = (ev.start - s) / r * config.height;
      e_e = (ev.end - s) / r * config.height;
      e_l = calculateAmount(viewport, ev, 1);
      triangle(e_s, e_e, e_l * config.width);
    }
    return drawLabels();
  };
  draw();
  (function() {
    var ctrlKey, lastDeltaY, lastDeltaYTime, lastY, mouseDown, ms_Slice, ms_Time, ms_Y, onMove, shiftKey, speed, speedTime, timeout1, timeout2, vs_Delta, vs_End, vs_Start;
    mouseDown = false;
    ms_Y = 0;
    ms_Time = 0;
    ms_Slice = 0;
    vs_End = 0;
    vs_Start = 0;
    vs_Delta = 0;
    shiftKey = false;
    ctrlKey = false;
    lastY = 0;
    speed = 0;
    speedTime = 0;
    lastDeltaY = 0;
    lastDeltaYTime = 0;
    timeout1 = 0;
    timeout2 = 0;
    canvas.on('mousedown', function(e) {
      mouseDown = true;
      ms_Y = e.offsetY;
      vs_End = viewport.end;
      vs_Start = viewport.start;
      vs_Delta = viewport.delta;
      shiftKey = e.shiftKey;
      ctrlKey = e.ctrlKey;
      ms_Time = Math.scale(vs_Start, vs_End, ms_Y / config.height);
      ms_Slice = ms_Y / config.height;
      clearTimeout(timeout2);
      clearTimeout(timeout1);
      timeout1 = 0;
      return timeout2 = 0;
    });
    canvas.on('mouseleave mouseup', function() {
      var act, interval;
      if (!mouseDown) {
        return;
      }
      mouseDown = false;
      if (speed !== 0) {
        clearTimeout(timeout2);
        clearTimeout(timeout1);
        timeout1 = 0;
        timeout2 = 0;
        interval = 1e3 / 40;
        speed = speed * interval / speedTime;
        act = function() {
          if (Math.abs(speed) >= 1) {
            lastY += speed;
            onMove(lastY, false);
            timeout2 = setTimeout(act, interval);
            return speed *= 0.8;
          } else {
            lastY = 0;
            speed = 0;
            speedTime = 0;
            lastDeltaY = 0;
            return lastDeltaYTime = 0;
          }
        };
        return timeout2 = setTimeout(act, interval);
      }
    });
    canvas.mousemove(function(e) {
      if (mouseDown) {
        return onMove(e.offsetY);
      }
    });
    return onMove = function(offsetY, interaction) {
      var deltaY, diff, ratio, unitY, vend, vstart;
      if (interaction == null) {
        interaction = true;
      }
      deltaY = offsetY - ms_Y;
      unitY = viewport.delta / config.height;
      if (interaction) {
        speed = offsetY - lastY;
        lastY = offsetY;
        speedTime = Date.now();
        speedTime -= lastDeltaYTime || speedTime;
        lastDeltaY = deltaY;
        lastDeltaYTime = Date.now();
        clearTimeout(timeout1);
        timeout1 = setTimeout((function() {
          return speed = 0;
        }), 100);
      }
      vstart = viewport.start;
      vend = viewport.end;
      if (ctrlKey) {
        ratio = (config.height - offsetY) / (config.height - ms_Y);
        vstart = vs_End - vs_Delta / ratio;
      }
      if (shiftKey) {
        ratio = offsetY / ms_Y;
        vend = vs_Start + vs_Delta / ratio;
      }
      if (!ctrlKey && !shiftKey) {
        vstart = vs_Start - (deltaY * unitY);
        vend = vs_End - (deltaY * unitY);
      }
      diff = vend - vstart;
      if (10 * minutes > diff || diff > 300 * years) {
        return;
      }
      viewport.start = vstart;
      viewport.end = vend;
      return draw();
    };
  })();
  return typeof window !== "undefined" && window !== null ? window.viewport = viewport : void 0;
});



},{"./dates.coffee":2,"./utils.coffee":3}],2:[function(require,module,exports){
var RangeScale, Task, TimeRange, Viewport, extend, identity, meta, ref, totime,
  extend1 = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

ref = require('./utils.coffee'), meta = ref.meta, extend = ref.extend, identity = ref.identity, totime = ref.totime;

TimeRange = (function() {
  function TimeRange(start1, end1, frozen) {
    this.start = start1;
    this.end = end1;
    this.frozen = frozen != null ? frozen : false;
  }

  TimeRange.prototype.compareTo = function(other) {};

  TimeRange.prototype.parse = function(needle) {
    return Math.scale(this._start, this._end, needle);
  };

  return TimeRange;

})();

meta(TimeRange, {
  start: {
    set: function(x) {
      delete this._level;
      delete this._delta;
      return this._start = totime(x);
    },
    get: function() {
      return this._start;
    }
  },
  end: {
    set: function(x) {
      delete this._level;
      delete this._delta;
      return this._end = totime(x);
    },
    get: function() {
      return this._end;
    }
  },
  level: {
    set: function(x) {
      if (!this.frozen) {
        return this._level = x;
      }
    },
    get: function() {
      return this._level || (this._level = RangeScale.getDurationScale(this.delta));
    }
  },
  delta: {
    set: function() {},
    get: function() {
      return this._delta || (this._delta = this.end - this.start);
    }
  }
});

Viewport = (function(superClass) {
  extend1(Viewport, superClass);

  function Viewport() {
    return Viewport.__super__.constructor.apply(this, arguments);
  }

  return Viewport;

})(TimeRange);

Task = (function(superClass) {
  extend1(Task, superClass);

  function Task(start, end) {
    Task.__super__.constructor.call(this, start, end, true);
  }

  return Task;

})(TimeRange);

RangeScale = (function() {
  function RangeScale(name1, unit1, scale1) {
    var extra;
    this.name = name1;
    this.unit = unit1;
    this.scale = scale1;
    extra = RangeScale.extras[this.name];
    if (extra != null) {
      extend(this, extra);
    }
    this.parse || (this.parse = this.name);
  }

  RangeScale.prototype.getRulers = function(range) {
    return this.doEachRuler(range, identity);
  };

  RangeScale.prototype.doEachRuler = function(range, fn) {
    var end, results, ruler, start, unit;
    start = range.start, end = range.end;
    unit = this.unit;
    ruler = start - start % unit;
    results = [];
    while ((ruler += unit) <= end) {
      results.push(fn(ruler));
    }
    return results;
  };

  RangeScale.prototype.first = false;

  RangeScale.prototype.last = false;

  RangeScale.prototype.format = function(range, time) {
    var m;
    m = moment(time);
    return m.format(this.pattern);
  };

  RangeScale.extras = {};

  RangeScale.scales = (function() {
    var counter, create, scales, yearRuler;
    counter = 0;
    scales = [];
    create = function(name, unit, extra) {
      var scale;
      RangeScale.extras[name] = extra;
      scale = new RangeScale(name, unit, ++counter);
      return scales.push(scale);
    };
    create("second", second, {
      pattern: '[]',
      first: true
    });
    create("minute", minute, {
      pattern: 'mm'
    });
    create("hour-slice", minute * 10, {
      pattern: 'HH:mm'
    });
    create("hour", hour, {
      pattern: 'HH'
    });
    create("half-day", day * 0.5, {
      pattern: 'A LL',
      doEachRuler: function(range, fn) {
        var end, obj, results, ruler, start;
        start = range.start, end = range.end;
        obj = new Date(start);
        obj.setMilliseconds(0);
        obj.setSeconds(0);
        obj.setHours(12 * Math.floor(obj.getHours() / 12));
        obj.setMinutes(0);
        ruler = 0;
        results = [];
        while ((ruler = obj.getTime()) <= end) {
          fn(ruler);
          results.push(obj.setHours(12 + obj.getHours()));
        }
        return results;
      }
    });
    create("day", day, {
      pattern: 'Do',
      doEachRuler: function(range, fn) {
        var end, obj, results, ruler, start;
        start = range.start, end = range.end;
        obj = new Date(start);
        obj.setMilliseconds(0);
        obj.setSeconds(0);
        obj.setHours(0);
        obj.setMinutes(0);
        ruler = 0;
        results = [];
        while ((ruler = obj.getTime()) <= end) {
          fn(ruler);
          results.push(obj.setDate(1 + obj.getDate()));
        }
        return results;
      }
    });
    create("week", week, {
      pattern: '[Week] wo',
      doEachRuler: function(range, fn) {
        var end, obj, results, ruler, start;
        start = range.start, end = range.end;
        obj = new Date(start);
        obj.setMilliseconds(0);
        obj.setSeconds(0);
        obj.setHours(0);
        obj.setMinutes(0);
        obj.setDate(obj.getDate() - obj.getDay() + 1);
        ruler = 0;
        results = [];
        while ((ruler = obj.getTime()) <= end) {
          fn(ruler);
          results.push(obj.setDate(7 + obj.getDate()));
        }
        return results;
      }
    });
    create("month", month, {
      pattern: 'MMMM YYYY',
      doEachRuler: function(range, fn) {
        var end, obj, results, ruler, start;
        start = range.start, end = range.end;
        obj = new Date(start);
        obj.setMilliseconds(0);
        obj.setSeconds(0);
        obj.setHours(0);
        obj.setMinutes(0);
        obj.setDate(1);
        ruler = 0;
        results = [];
        while ((ruler = obj.getTime()) <= end) {
          fn(ruler);
          results.push(obj.setMonth(1 + obj.getMonth()));
        }
        return results;
      }
    });
    create("quarter", month * 3, {
      pattern: 'MMMM YYYY',
      doEachRuler: function(range, fn) {
        var end, obj, results, ruler, start;
        start = range.start, end = range.end;
        obj = new Date(start);
        obj.setMilliseconds(0);
        obj.setSeconds(0);
        obj.setHours(0);
        obj.setMinutes(0);
        obj.setDate(1);
        obj.setMonth(3 * Math.floor(obj.getMonth() / 3));
        ruler = 0;
        results = [];
        while ((ruler = obj.getTime()) <= end) {
          fn(ruler);
          results.push(obj.setMonth(3 + obj.getMonth()));
        }
        return results;
      }
    });
    yearRuler = function(count, extra) {
      return extend({
        pattern: 'YYYY',
        doEachRuler: function(range, fn) {
          var end, obj, results, ruler, start;
          start = range.start, end = range.end;
          obj = new Date(start);
          obj.setMilliseconds(0);
          obj.setSeconds(0);
          obj.setHours(0);
          obj.setMinutes(0);
          obj.setDate(1);
          obj.setMonth(0);
          if (count > 1) {
            obj.setFullYear(count * Math.floor(obj.getFullYear() / count));
          }
          ruler = 0;
          results = [];
          while ((ruler = obj.getTime()) <= end) {
            fn(ruler);
            results.push(obj.setFullYear(count + obj.getFullYear()));
          }
          return results;
        }
      }, extra || {});
    };
    create("year", year, yearRuler(1));
    create("lustrum", year * 5, yearRuler(5));
    create("decade", year * 10, yearRuler(10));
    create("century", year * 100, yearRuler(100, {
      last: true
    }));
    return scales;
  })();

  RangeScale.getDurationScale = function(d) {
    var i, len, next, nextV, prev, prevV, ref1, test;
    prev = RangeScale.scales[RangeScale.scales.length - 1];
    next = null;
    ref1 = RangeScale.scales;
    for (i = 0, len = ref1.length; i < len; i++) {
      test = ref1[i];
      if (d < test.unit) {
        next = test;
        break;
      }
      prev = test;
    }
    if (prev.last) {
      return prev;
    }
    prevV = prev.unit;
    nextV = next.unit;
    return new RangeScale(prev.name, prevV, prev.scale + (Math.scale(prevV, nextV, d)));
  };

  RangeScale.getByScale = function(scale) {
    return this.scales[scale - 1];
  };

  RangeScale.getPrevNext = function(scale) {
    var index;
    index = Math.floor(scale.scale || scale);
    return [this.getByScale(index - 1), this.getByScale(index + 1)];
  };

  return RangeScale;

})();

extend(exports, {
  TimeRange: TimeRange,
  RangeScale: RangeScale,
  Viewport: Viewport,
  Task: Task
});



},{"./utils.coffee":3}],3:[function(require,module,exports){
var global=typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {};var callOn, clamp, createLog, durationScale, extend, fromnow, getDurationScale, getFromDurationScale, getScale, identity, log10, meta, root, scale, totime, unscale;

root = typeof global !== "undefined" && global !== null ? global : window;

extend = function(target, source) {
  var key;
  for (key in source) {
    target[key] = source[key];
  }
  return target;
};

meta = function(target, obj) {
  return Object.defineProperties(target.prototype, obj);
};

callOn = function(o, f) {
  return f.apply(o, Array.prototype.slice.call(arguments, 1, -1));
};

identity = function(o) {
  return o;
};

createLog = function(o, b) {
  return (function(x) {
    return this.log(x) / this.log(b);
  }).bind(o);
};

log10 = function(x) {
  return (this.log(x)) / (this.log(10));
};

clamp = function(min, max, n) {
  return this.max(min, this.min(max, n));
};

scale = function(min, max, n) {
  var range, value;
  if (min > max) {
    return 1 - this.scale(max, min, n);
  } else {
    range = max - min;
    value = n - min;
    return value / range;
  }
};

unscale = function(min, max, s) {
  return (max - min) * s + min;
};

callOn(Math, function() {
  this.clamp = clamp.bind(this);
  this.scale = scale.bind(this);
  this.unscale = unscale.bind(this);
  this.log10 = log10.bind(this);
  this.log3 = createLog(this, 3);
  this.log12 = createLog(this, 12);
  return this.log24 = createLog(this, 24);
});

callOn(root, function() {
  this.second = this.seconds = 1e3;
  this.minute = this.minutes = 60 * second;
  this.hour = this.hours = 60 * minute;
  this.day = this.days = 24 * hour;
  this.week = this.weeks = 7 * day;
  this.month = this.months = 30 * day;
  return this.year = this.years = 365 * day;
});

fromnow = function(x) {
  return x + Date.now();
};

totime = function(d) {
  return (new Date(d)).getTime();
};

durationScale = (function() {
  var counter, describe, scales;
  counter = 0;
  scales = [];
  describe = function(name, unit) {
    return scales.push({
      name: name,
      unit: unit,
      scale: ++counter
    });
  };
  describe("second", second);
  describe("minute", minute);
  describe("hour-slice", minute * 10);
  describe("hour", hour);
  describe("half-day", day * 0.5);
  describe("day", day);
  describe("week", week);
  describe("month", month);
  describe("quarter", month * 3);
  describe("year", year);
  describe("lustrum", year * 5);
  describe("decade", year * 10);
  describe("century", year * 100);
  return scales;
})();

getScale = function(i) {
  return durationScale[i - 1];
};

getDurationScale = function(d) {

  /* d = totime d */
  var j, len, next, nextV, prev, prevV, result, test;
  prev = null;
  next = null;
  for (j = 0, len = durationScale.length; j < len; j++) {
    test = durationScale[j];
    if (d < test.unit) {
      next = test;
      break;
    }
    prev = test;
  }
  prevV = prev.unit;
  nextV = next.unit;
  return result = {
    name: prev.name,
    unit: prev.unit,
    scale: prev.scale + (Math.scale(prevV, nextV, d))
  };
};

getFromDurationScale = function(s) {
  var next, prev;
  scale = Math.floor(s);
  prev = getScale(scale);
  next = getScale(scale + 1);
  return Math.floor(Math.unscale(prev.unit, next.unit, s % 1));
};


/*
	doexports {doexports, totime, fromnow, getDurationScale, getFromDurationScale, meta, callOn}
 */

extend(module.exports, {
  extend: extend,
  identity: identity,
  totime: totime,
  fromnow: fromnow,
  getScale: getScale,
  getDurationScale: getDurationScale,
  getFromDurationScale: getFromDurationScale,
  meta: meta,
  callOn: callOn
});



},{}]},{},[1])