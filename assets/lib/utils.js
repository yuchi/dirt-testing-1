// Generated by CoffeeScript 1.7.1
(function() {
  var callOn, clamp, createLog, durationScale, extend, fromnow, getDurationScale, getFromDurationScale, getScale, identity, log10, meta, root, scale, totime, unscale;

  root = typeof global !== "undefined" && global !== null ? global : window;

  extend = function(target, source) {
    var key;
    for (key in source) {
      target[key] = source[key];
    }
    return target;
  };

  meta = function(target, obj) {
    return Object.defineProperties(target.prototype, obj);
  };

  callOn = function(o, f) {
    return f.apply(o, Array.prototype.slice.call(arguments, 1, -1));
  };

  identity = function(o) {
    return o;
  };

  createLog = function(o, b) {
    return (function(x) {
      return this.log(x) / this.log(b);
    }).bind(o);
  };

  log10 = function(x) {
    return (this.log(x)) / (this.log(10));
  };

  clamp = function(min, max, n) {
    return this.max(min, this.min(max, n));
  };

  scale = function(min, max, n) {
    var range, value;
    if (min > max) {
      return 1 - this.scale(max, min, n);
    } else {
      range = max - min;
      value = n - min;
      return value / range;
    }
  };

  unscale = function(min, max, s) {
    return (max - min) * s + min;
  };

  callOn(Math, function() {
    this.clamp = clamp.bind(this);
    this.scale = scale.bind(this);
    this.unscale = unscale.bind(this);
    this.log10 = log10.bind(this);
    this.log3 = createLog(this, 3);
    this.log12 = createLog(this, 12);
    return this.log24 = createLog(this, 24);
  });

  callOn(root, function() {
    this.second = this.seconds = 1e3;
    this.minute = this.minutes = 60 * second;
    this.hour = this.hours = 60 * minute;
    this.day = this.days = 24 * hour;
    this.week = this.weeks = 7 * day;
    this.month = this.months = 30 * day;
    return this.year = this.years = 365 * day;
  });

  fromnow = function(x) {
    return x + Date.now();
  };

  totime = function(d) {
    return (new Date(d)).getTime();
  };

  durationScale = (function() {
    var counter, describe, scales;
    counter = 0;
    scales = [];
    describe = function(name, unit) {
      return scales.push({
        name: name,
        unit: unit,
        scale: ++counter
      });
    };
    describe("second", second);
    describe("minute", minute);
    describe("hour-slice", minute * 10);
    describe("hour", hour);
    describe("half-day", day * 0.5);
    describe("day", day);
    describe("week", week);
    describe("month", month);
    describe("quarter", month * 3);
    describe("year", year);
    describe("lustrum", year * 5);
    describe("decade", year * 10);
    describe("century", year * 100);
    return scales;
  })();

  getScale = function(i) {
    return durationScale[i - 1];
  };

  getDurationScale = function(d) {

    /* d = totime d */
    var next, nextV, prev, prevV, result, test, _i, _len;
    prev = null;
    next = null;
    for (_i = 0, _len = durationScale.length; _i < _len; _i++) {
      test = durationScale[_i];
      if (d < test.unit) {
        next = test;
        break;
      }
      prev = test;
    }
    prevV = prev.unit;
    nextV = next.unit;
    return result = {
      name: prev.name,
      unit: prev.unit,
      scale: prev.scale + (Math.scale(prevV, nextV, d))
    };
  };

  getFromDurationScale = function(s) {
    var next, prev;
    scale = Math.floor(s);
    prev = getScale(scale);
    next = getScale(scale + 1);
    return Math.floor(Math.unscale(prev.unit, next.unit, s % 1));
  };


  /*
  	doexports {doexports, totime, fromnow, getDurationScale, getFromDurationScale, meta, callOn}
   */

  extend(module.exports, {
    extend: extend,
    identity: identity,
    totime: totime,
    fromnow: fromnow,
    getScale: getScale,
    getDurationScale: getDurationScale,
    getFromDurationScale: getFromDurationScale,
    meta: meta,
    callOn: callOn
  });

}).call(this);
