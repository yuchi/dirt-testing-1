
should = require 'should'

require '../src/utils'
require '../src/classes'

# Ranges tests
# ============

describe 'Utils', ->

	describe 'Math', ->

		describe '#log10', ->

			it 'should give 2 for 100', ->

				2.should.be.equal Math.log10 100

			it 'should give 0 for 1', ->

				0.should.be.equal Math.log10 1

	describe 'Time', ->

		describe '#fromnow', ->

			now = Date.now

			it "should be equal to #{now}", ->

				(do now).should.be.equal fromnow 0

			it "should be equal to tomorrow", ->

				(1*day + do now).should.be.equal fromnow 1*day


describe 'TimeRange', ->

	now = do Date.now
	zeroDays = now
	oneDay = now + 1*day
	tenDays = now + 10*days

	it 'should be exported', ->
		
		should.exist global.TimeRange

	describe '#creation', ->

		range = new TimeRange now, tenDays

		it 'sample range should have a level of 6', ->

			range.level.should.be.equal 6

		it 'sample range should now have a level of 5', ->
			
			range.end = oneDay

			range.level.should.be.equal 5
