# vim: tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab

{meta, extend, identity, totime} = require './utils.coffee'


# Dates
# =====


# Classes
# -------

class TimeRange

	constructor: (@start, @end, @frozen = false) ->

	compareTo: (other) ->

	parse: (needle) -> Math.scale @_start, @_end, needle


meta TimeRange,

	start:
		set: (x) ->
			delete @_level
			delete @_delta
			@_start = totime x
		get: -> @_start

	end:
		set: (x) ->
			delete @_level
			delete @_delta
			@_end = totime x
		get: -> @_end

	level:
		set: (x) -> @_level = x unless @frozen
		get: -> @_level or= RangeScale.getDurationScale @delta

	delta:
		set: ->
		get: -> @_delta or= @end - @start


class Viewport extends TimeRange


class Task extends TimeRange

	constructor: (start, end) -> super start, end, true


class RangeScale

	constructor: (@name, @unit, @scale) ->
		extra = RangeScale.extras[@name]
		extend this, extra if extra?
		@parse or= @name

	getRulers: (range) -> @doEachRuler range, identity

	doEachRuler: (range, fn) ->
		{start, end} = range
		{unit} = this
		ruler = start - start % unit
		fn ruler while (ruler += unit) <= end

	first: false

	last: false

	format: (range, time) ->
		m = moment time
		m.format @pattern

	@extras = {}

	@scales = do =>

		counter = 0
		scales = []

		create = (name, unit, extra) =>
			@extras[name] = extra
			scale = new RangeScale name, unit, ++counter
			scales.push scale

		create "second", second, pattern: '[]', first: true

		create "minute", minute, pattern: 'mm'

		create "hour-slice", minute * 10, pattern: 'HH:mm'

		create "hour", hour, pattern: 'HH'

		create "half-day", day * 0.5,
			pattern: 'A LL'
			doEachRuler: (range, fn) ->
				{start, end} = range
				obj = new Date start

				obj.setMilliseconds 0
				obj.setSeconds 0
				obj.setHours 12 * Math.floor obj.getHours() / 12
				obj.setMinutes 0

				ruler = 0
				while (ruler = do obj.getTime) <= end
					fn ruler# if ruler >= start
					obj.setHours 12 + do obj.getHours

		create "day", day,
			pattern: 'Do'
			doEachRuler: (range, fn) ->
				{start, end} = range
				obj = new Date start

				obj.setMilliseconds 0
				obj.setSeconds 0
				obj.setHours 0
				obj.setMinutes 0

				ruler = 0
				while (ruler = do obj.getTime) <= end
					fn ruler# if ruler >= start
					obj.setDate 1 + do obj.getDate

		create "week", week,
			pattern: '[Week] wo'
			doEachRuler: (range, fn) ->
				{start, end} = range
				obj = new Date start

				obj.setMilliseconds 0
				obj.setSeconds 0
				obj.setHours 0
				obj.setMinutes 0
				obj.setDate obj.getDate() - obj.getDay() + 1

				ruler = 0
				while (ruler = do obj.getTime) <= end
					fn ruler# if ruler >= start
					obj.setDate 7 + do obj.getDate

		create "month", month,
			pattern: 'MMMM YYYY'
			doEachRuler: (range, fn) ->
				{start, end} = range
				obj = new Date start

				obj.setMilliseconds 0
				obj.setSeconds 0
				obj.setHours 0
				obj.setMinutes 0
				obj.setDate 1

				ruler = 0
				while (ruler = do obj.getTime) <= end
					fn ruler# if ruler >= start
					obj.setMonth 1 + do obj.getMonth

		create "quarter", month * 3,
			pattern: 'MMMM YYYY'
			doEachRuler: (range, fn) ->
				{start, end} = range
				obj = new Date start

				obj.setMilliseconds 0
				obj.setSeconds 0
				obj.setHours 0
				obj.setMinutes 0
				obj.setDate 1
				obj.setMonth 3 * Math.floor obj.getMonth() / 3

				ruler = 0
				while (ruler = do obj.getTime) <= end
					fn ruler# if ruler >= start
					obj.setMonth 3 + do obj.getMonth

		yearRuler = (count, extra) ->
			return extend {
				pattern: 'YYYY'
				doEachRuler: (range, fn) ->
					{start, end} = range
					obj = new Date start

					obj.setMilliseconds 0
					obj.setSeconds 0
					obj.setHours 0
					obj.setMinutes 0
					obj.setDate 1
					obj.setMonth 0

					if count > 1
						obj.setFullYear count * Math.floor obj.getFullYear() / count

					ruler = 0
					while (ruler = do obj.getTime) <= end
						fn ruler# if ruler >= start
						obj.setFullYear count + do obj.getFullYear
			}, extra || {}

		create "year", year, yearRuler 1
		create "lustrum", year * 5, yearRuler 5
		create "decade", year * 10, yearRuler 10
		create "century", year * 100, yearRuler 100, last: true

		scales

	@getDurationScale = (d) =>
		prev = @scales[@scales.length-1]
		next = null
		for test in @scales
			if d < test.unit
				next = test
				break
			prev = test

		if prev.last
			return prev

		prevV = prev.unit
		nextV = next.unit

		new RangeScale prev.name, prevV, prev.scale + (Math.scale prevV, nextV, d)

	@getByScale = (scale) -> @scales[scale - 1]

	@getPrevNext = (scale) ->
		index = Math.floor scale.scale||scale
		[
			@getByScale index-1
			@getByScale index+1
		]

# Utils
# -----



extend exports, {TimeRange, RangeScale, Viewport, Task}
