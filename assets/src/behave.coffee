# vim: tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab

$ ->

	# Canvas
	# ======

	config =
		width: 320
		height: 480

	canvas = $ '#canvas'

	scaleText = $ '#scale'

	canvas.attr
		width: config.width
		height: config.height

	context = canvas.get(0).getContext '2d'

	context.fillStyle = 'rgba(200,0,0,0.1)'

	contextCache = null
	labelsBuffer = null

	do clearCache = ->
		labelsBuffer = {}
		contextCache = {}

	triangle = (s, e, l) ->
		context.fillStyle = 'rgba(200,0,0,0.1)'
		#console.log 'triangle', arguments
		#context.fillRect 0, s, l, e - s
		do context.beginPath
		context.moveTo config.width, s
		context.lineTo config.width - l, e
		context.lineTo config.width, e
		do context.fill
		do context.closePath

	ruler = (y, size, color = '#111') ->
		y = Math.floor y
		width = size * config.width
		context.fillStyle = color
		context.fillRect 0, y, width * 0.4, 1
		###
		do context.beginPath
		context.moveTo 0, y
		context.lineTo config.width * 0.1, y
		config.
		###

	label = (y, size, text, color = '#333') ->
		y = Math.floor y
		size *= 40
		unless contextCache[y] >= size
			contextCache[y] = size
			labelsBuffer[y] =
				font: size + 'px sans-serif'
				fillStyle: color
				fillText: [text, 1.5*size, y + size]

	drawLabels = ->
		for y of labelsBuffer
			cur = labelsBuffer[y]
			context.font = cur.font
			context.fillStyle = cur.fillStyle
			context.fillText cur.fillText...

	# Imports
	# =======

	{getScale} = require './utils.coffee'
	{RangeScale, Viewport, Task} = require './dates.coffee'

	# Time
	# ====

	log10 = (x) -> Math.log(x) / Math.log(10)

	tonow = (x) -> Date.now() + x

	second = 1e3
	minute = 60 * second
	hour = 60 * minute
	day = 24 * hour

	getZoomLevel = (r) ->
		elta = r.end - r.start
		delta /= 10 * day
		6 + log10 delta

	getZoomRange = (v) ->
		zooml = getZoomLevel v
		start: zooml - 1
		end: zooml

	###
	viewport =
		start: tonow -10 * day
		end: tonow 10 * day

	viewport.range = getZoomRange viewport
	###

	viewport = new Viewport (tonow -10*days), (tonow 10*day)

	clamp = (mi, ma, x) -> Math.max mi, Math.min ma, x

	calculateAmount = (v, ev, margin = 1) ->
		###
		s = v.range.start
		e = v.range.end
		delta_s = s - ev.level
		delta_e = ev.level - e
		if s <= ev.level <= e
			1
		else if s > ev.level
			clamp 0, 1, 1 - (delta_s / margin)
		else if e < ev.level
			clamp 0, 1, 1 - (delta_e / margin)
		else
			0
		###
		delta = ev.level.scale - v.level.scale
		min = -1 - margin
		max = 0 + margin
		if delta < min or delta > max
			0
		else if -1 <= delta <= 0
			1
		else if delta > 0
			1 - delta
		else
			Math.scale min, -1, delta



	event = (s, e) ->
		s = tonow s
		e = tonow e
		if e < s
			tmp = e
			e = s
			s = tmp
		new Task s, e
	###
		obj =
			start: tonow s
			end: tonow e
		obj.level = getZoomLevel obj
		obj
	###

	###
	events = [
		(event 1*day, 3*day)
		(event 2*day, 4*day)
			(event -2*day, 15*day)
	]
	###
	rand = (x) -> ((x/-2)+Math.random()*Math.abs(x))
	eventsCount = 30
	events = while eventsCount--
		event day*rand(600), day*rand(600)

	force = 1

	updateViewport = ->
		viewport.start -= force*hour*2
		viewport.end += force*hour*10
		viewport.range = getZoomRange viewport

	($ document.body).on 'click', -> force = - force

	count = 0
	draw = ->
		do clearCache
		context.clearRect 0, 0, config.width, config.height
		#events[0].end -= 10*minute
		#events[1].start -= 6*hour
		#events[0].level = getZoomLevel events[0]
		#events[1].level = getZoomLevel events[1]
		#do updateViewport

		{level, delta} = viewport
		amount = level.scale % 1
		unitPX = config.height/delta

		# Amount
		# - 0 ------- 0.5 ------- 1 - #
		# ##OO       OOOO       OOOO  # 1 Prev
		# ####       ###O       ##OO  # 2 Actual
		# OOOO       ##OO       ####  # 3 Next

		scaleText.text level.name

		#SCARPONPUBMESTRE01052012

		amount1 = 0
		amount2 = 0
		amount3 = 0
		do ->
			min = Math.min
			max = Math.max
			amount1 = 0.5 - min(0.5, amount)
			amount2 = 0.5 + (1 - amount) * 0.5
			amount3 = amount

		#prev = getScale -1 + Math.floor level.scale
		#next = getScale 1 + Math.floor level.scale

		if debug?
			for i in [0..3]
				ruler 100 + i, amount, 'rgba(0,0,255,0.3)'

		[prev, next] = RangeScale.getPrevNext level

		time2px = (t) -> config.height * viewport.parse(t)

		if prev
			prev.doEachRuler viewport, (t) ->
				positionY = time2px t
				label positionY, amount1, prev.format viewport, t
				ruler positionY, amount1

		level.doEachRuler viewport, (t) ->
			positionY = time2px t
			label positionY, amount2, level.format viewport, t
			ruler positionY, amount2

		if next
			next.doEachRuler viewport, (t) ->
				positionY = time2px t
				label positionY, amount3, next.format viewport, t
				ruler positionY, amount3


		for ev in events
			s = viewport.start
			e = viewport.end
			r = e - s
			e_s = (ev.start - s) / r * config.height
			e_e = (ev.end - s) / r * config.height
			e_l = calculateAmount viewport, ev, 1
			#console.log e_l if e_l < 1
			triangle e_s, e_e, e_l * config.width

		do drawLabels

	do draw

	do ->
		mouseDown = false
		ms_Y = 0
		ms_Time = 0
		ms_Slice = 0
		vs_End = 0
		vs_Start = 0
		vs_Delta = 0
		shiftKey = false
		ctrlKey = false

		lastY = 0
		speed = 0
		speedTime = 0
		lastDeltaY = 0
		lastDeltaYTime = 0

		timeout1 = 0
		timeout2 = 0

		canvas.on 'mousedown', (e) ->
			mouseDown = true
			ms_Y = e.offsetY
			vs_End = viewport.end
			vs_Start = viewport.start
			vs_Delta = viewport.delta
			shiftKey = e.shiftKey
			ctrlKey = e.ctrlKey

			ms_Time = Math.scale vs_Start, vs_End, ms_Y / config.height
			ms_Slice = ms_Y / config.height

			clearTimeout timeout2
			clearTimeout timeout1
			timeout1 = 0
			timeout2 = 0

		canvas.on 'mouseleave mouseup', ->
			return if !mouseDown
			mouseDown = false
			if speed != 0
				clearTimeout timeout2
				clearTimeout timeout1
				timeout1 = 0
				timeout2 = 0

				interval = 1e3/40
				speed = speed * interval / speedTime

				act = ->
					if Math.abs(speed) >= 1
						lastY += speed
						onMove lastY, false
						timeout2 = setTimeout act, interval
						speed *= 0.8
					else
						lastY = 0
						speed = 0
						speedTime = 0
						lastDeltaY = 0
						lastDeltaYTime = 0

				timeout2 = setTimeout act, interval

		canvas.mousemove (e) ->
			if mouseDown
				onMove e.offsetY

		onMove = (offsetY, interaction = true) ->
			deltaY = offsetY - ms_Y
			unitY = viewport.delta / config.height

			if interaction
				speed = offsetY - lastY
				lastY = offsetY
				speedTime = do Date.now
				speedTime -= lastDeltaYTime or speedTime
				lastDeltaY = deltaY
				lastDeltaYTime = do Date.now
				clearTimeout timeout1
				timeout1 = setTimeout (-> speed = 0), 100

			vstart = viewport.start
			vend = viewport.end

			if ctrlKey
				ratio = (config.height - offsetY) / (config.height - ms_Y)
				vstart = vs_End - vs_Delta / ratio

			if shiftKey
				ratio = offsetY / ms_Y
				vend = vs_Start + vs_Delta / ratio

			if not ctrlKey and not shiftKey
				vstart = vs_Start - (deltaY * unitY)
				vend = vs_End - (deltaY * unitY)

			diff = vend - vstart
			if 10*minutes > diff or diff > 300*years
				return

			viewport.start = vstart
			viewport.end = vend

			do draw


	window?.viewport = viewport
