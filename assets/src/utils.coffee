# vim: tabstop=4:softtabstop=4:shiftwidth=4:noexpandtab 

# Lang
# ====

	# The root (global)
	root = if global? then global else window

	extend = (target, source) ->
		for key of source
			target[ key ] = source[ key ]
		target

	# Metaprogramming util
	meta = (target, obj) -> Object.defineProperties target::, obj

	# Functional Function::prototype
	callOn = (o, f) -> f.apply o, Array::slice.call(arguments, 1, -1)

	# Identity...
	identity = (o) -> o

# Math
# ====

	createLog = (o, b) -> ((x) -> @log(x) / @log(b)).bind o

	# Base 10 logarithm
	log10 = (x) -> (@log x) / (@log 10)

	# Clamps the value so that `min < n < max`
	clamp = (min, max, n) -> @max min, @min max, n

	scale = (min, max, n) ->
		if min > max
			return 1 - @scale max, min, n
		else
			range = max - min
			value = n - min
			value / range

	unscale = (min, max, s) -> (max - min) * s + min

	callOn Math, ->
		@clamp = clamp.bind @
		@scale = scale.bind @
		@unscale = unscale.bind @

		@log10 = log10.bind @
		@log3 = createLog @, 3
		@log12 = createLog @, 12
		@log24 = createLog @, 24

# Time
# ====

# Constants
# ---------

	callOn root, ->
		@second = @seconds = 1e3
		@minute = @minutes = 60 * second
		@hour   = @hours   = 60 * minute
		@day    = @days    = 24 * hour
		@week   = @weeks   = 7  * day
		@month  = @months  = 30 * day
		@year   = @years   = 365 * day


# Functions
# ---------

	# Gets a new timestamp from now
	fromnow = (x) -> x + do Date.now

	# Gets a Date or Number and returns a Number
	totime = (d) -> do (new Date d).getTime

	durationScale = do ->
		counter = 0
		scales = []
		describe = (name, unit) -> scales.push { name, unit, scale: ++counter }
		describe "second"      , second
		describe "minute"      , minute
		describe "hour-slice"  , minute * 10
		describe "hour"        , hour
		describe "half-day"    , day * 0.5
		describe "day"         , day
		describe "week"        , week
		describe "month"       , month
		describe "quarter"     , month * 3
		describe "year"        , year
		describe "lustrum"     , year * 5
		describe "decade"      , year * 10
		describe "century"     , year * 100
		scales

	getScale = (i) -> durationScale[ i-1 ]

	# ...
	getDurationScale = (d) ->
		### d = totime d ###
		prev = null
		next = null
		for test in durationScale
			if d < test.unit
				next = test
				break
			prev = test

		prevV = prev.unit
		nextV = next.unit

		result = {
			name: prev.name
			unit: prev.unit
			scale: prev.scale + (Math.scale prevV, nextV, d)
		}

	getFromDurationScale = (s) ->
		scale = Math.floor s
		prev = getScale scale
		next = getScale scale + 1

		return Math.floor Math.unscale prev.unit, next.unit, s%1

# Exports
# =======

	###
	doexports {doexports, totime, fromnow, getDurationScale, getFromDurationScale, meta, callOn}
	###
	extend module.exports, {extend, identity, totime, fromnow, getScale, getDurationScale, getFromDurationScale, meta, callOn}
